/*
    Assignment: Practicing with Starbucks OOD
    Date: 02/21/2024
    Group 4:
        Christian Espinoza
        Lukas Hammett
        David Reyes
        Mohammad Nawid Wafa
*/

#include <fstream>
#include <regex>
#include "record.h"

vector<Record> data;
vector<Record> county_data;
string county;

void get_county_info() {
    cout << endl << "Please enter a county in California to search: ";
    getline(cin, county);
    county += " County";
    // cout << "TEST: county += \" County\": " << (county += " County") << endl;
    for (Record item : data) {
        if (item.get_county() == county) {
            county_data.push_back(item);
        }
    }
}

int count_ownership(vector<Record> given_data) {
    int count = 0;
    for (Record item : given_data) {
        if (item.get_ownership() == "Licensed") {
            count++;
        }
    }
    return count;
}

Record find_farthest_north(vector<Record> given_data) {
    Record max_record;
    double max = 0;
    for (Record item : given_data) {
        if (item.get_latitude() > max) {
            max = item.get_latitude();
            max_record = item;
        }
    }
    return max_record;
}

Record find_most_extras(vector<Record> given_data) {
    Record max_record;
    int max_extras = 0;
    for (Record item : given_data) {
        int extras = item.get_open_24_hours() + item.get_clover_brew() + item.get_warmed_food() + item.get_free_wifi() + item.get_verismo_system()
        + item.get_mobile_pay() + item.get_rewards() + item.get_la_boulange() + item.get_sodas() + item.get_drive_thru();
        if (extras > max_extras) {
            max_extras = extras;
            max_record = item;
        }
    }
    return max_record;
}

void percent_closed_on_weekends() {
  double sat_count = 0;
  double sun_count = 0;
  for (Record item : data) {
    if (item.get_sat_hours() == "Closed") {
      sat_count++;
    }
    if (item.get_sun_hours() == "Closed") {
      sun_count++;
    }
  }
  fprintf(stdout, "Percent of stores closed on Saturdays: %.2f%%\n", (sat_count / data.size() * 100));
  fprintf(stdout, "Percent of stores closed on Sundays: %.2f%%\n", (sun_count / data.size() * 100));
}

void phone_number_parentheses() {
    int count = 0;
    for (Record item: data) {
        if (regex_search(item.get_phone_number(), regex("[()]"))) {
            count++;
        }
    }
    cout << "Number of stores with parentheses in their phone numbers: " << count << endl;
}

int main() {
    fstream file;
    file.open("./StarbucksInCalifornia.csv", ios_base::in);
    if (!file.is_open()) {
        cout << "Could not find StarbucksInCalifornia.csv." << endl;
        return 1;
    }

    string line;
    // Skips the first line of the CSV (in other words, ignore the header row)
    getline(file, line);
    // Put each line of the CSV into the array of Records
    while (getline(file, line)) {
        data.push_back(Record(line));
    }
    file.close();

    // Q4: Percent of Starbucks stores in California closed on Saturdays and Sundays
    percent_closed_on_weekends();
    // Q6: Number of Starbucks stores that have parentheses in their phone numbers
    phone_number_parentheses();
    print_barrier();
    get_county_info();
    if (county_data.empty()) {
        cout << endl << "Could not find a county with that name." << endl;
        return 1;
    }

    // Q1: Print all records with a certain county name
    for (Record item : county_data) {
        item.print();
    }

    // Q2: Number of Starbucks stores in that county that have the Licensed ownership type
    cout << "Number of stores in this county that are Licensed: " << count_ownership(county_data) << endl;

    // Q3: Starbucks store in that county that is the farthest north (i.e. greatest latitude value)
    Record farthest_north = find_farthest_north(county_data);
    cout << endl << "The store in this county that is the farthest north is" << endl << "Starbucks Store #" << farthest_north.get_id()
    << " | " << farthest_north.get_name() << endl << "with a latitude of " << farthest_north.get_latitude() << endl;
    
    // Q5: Starbucks store in that county that has the most extra services available
    cout << endl << "The store in this county with the most extra services available is:" << endl;
    find_most_extras(county_data).print();
    return 0;
}

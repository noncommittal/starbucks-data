# OOD-with-Starbucks-data

For this problem we will read our data into a Vector data structure as StarbucksRecord objects.  We will need to create the StarbucksRecord class, as well as our test class.  We will then think of questions to ask the data, you are to write at least 3 methods to answer at least 3 questions about this data set, and output the results.  

Here are some questions, you can use 3 of these as your questions if you want to, or ask different questions:

* Print out the first record that has a certain County name. Then print out all of the records that have that County name. (we answered this one in lecture)

* How many Starbucks are in a certain County and have a given Ownership type of "Licensed"?

* Which Starbucks in Merced County is the farthest North?

* What percent of Starbucks are closed on Saturdays? Sundays?

* Which Starbucks in a certain County has the most extra services available(eg. 24-Hour Service, Oven-warmed food, etc.)?

* How many Starbucks stores stored their phone number using parentheses? 

Turn in your 2 C++ source code files - the object class to create records, and the test class to make an Vector of our objects to ask it questions. You should have at least 3 methods that answer at least 3 questions about the data, make sure to:

* put comments
* indent your code correctly
* put your name is at the top of the code as comments
* turn in your completed code via the github classroom, in your repo, by the due date

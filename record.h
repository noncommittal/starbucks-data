#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

void print_barrier() {
    cout << "─────────────────────────" << endl;
}

class Record {
    private:
        int id;
        string name;
        string ownership;
        string phone_number;
        string address;
        string city;
        string county;
        int zip;
        string coordinates;
        double latitude;
        //string longitude;
        string state;
        string image;
        string reg_hours;
        string sat_hours;
        string sun_hours;
        bool is_open_24_hours;
        bool has_clover_brew;
        bool has_warmed_food;
        bool has_free_wifi;
        bool has_verismo_system;
        bool allows_mobile_pay;
        bool has_rewards;
        bool has_la_boulange;
        bool has_sodas;
        bool has_drive_thru;
        string link;

    public:
        int get_id() { return id; }
        string get_name() { return name; }
        string get_ownership() { return ownership; }
        string get_phone_number() { return phone_number; }
        string get_address() { return address; }
        string get_city() { return city; }
        string get_county() { return county; }
        int get_zip() { return zip; }
        string get_coordinates() { return coordinates; }
        double get_latitude() { return latitude; }
        //string get_longitude() { return longitude; }
        string get_state() { return state; }
        string get_image() { return image; }
        string get_reg_hours() { return reg_hours; }
        string get_sat_hours() { return sat_hours; }
        string get_sun_hours() { return sun_hours; }
        bool get_open_24_hours() { return is_open_24_hours; }
        bool get_clover_brew() { return has_clover_brew; }
        bool get_warmed_food() { return has_warmed_food; }
        bool get_free_wifi() { return has_free_wifi; }
        bool get_verismo_system() { return has_verismo_system; }
        bool get_mobile_pay() { return allows_mobile_pay; }
        bool get_rewards() { return has_rewards; }
        bool get_la_boulange() { return has_la_boulange; }
        bool get_sodas() { return has_sodas; }
        bool get_drive_thru() { return has_drive_thru; }
        string get_link() { return link; }

        Record();
        Record(string& data);

        void print() {
            print_barrier();
            cout << "Starbucks Store #" << id << " | " << name << endl;
            cout << "Ownership Status: " << ownership;
            cout << " | Phone Number: " << phone_number << endl;
            cout << "Address: " << address << endl;
            cout << "Coordinates: " << coordinates << endl;
            //cout << "Coordinates: " << latitude << ", " << longitude << endl;
            cout << "Hours: (M-F) " << reg_hours;
            cout << (is_open_24_hours ? " (Open 24 Hours)" : "");
            cout << endl << "       (Sat) " << sat_hours << endl << "       (Sun) " << sun_hours << endl;
            cout << (has_clover_brew ? "✓ Clover Brew\n" : "");
            cout << (has_warmed_food ? "✓ Oven-Warmed Food\n" : "");
            cout << (has_free_wifi ? "✓ Free WiFi\n" : "");
            cout << (has_verismo_system ? "✓ Verismo System\n" : "");
            cout << (allows_mobile_pay ? "✓ Mobile Payment Accepted\n" : "");
            cout << (has_rewards ? "✓ Digital Rewards System\n" : "");
            cout << (has_la_boulange ? "✓ La Boulange\n" : "");
            cout << (has_sodas ? "✓ Fizzio Handcrafted Sodas\n" : "");
            cout << (has_drive_thru ? "✓ Drive Thru\n" : "");
            cout << endl << "Image: " << image << endl << "Store Link: " << link << endl << endl;
            print_barrier();
        }
};

Record::Record() { this -> id = -1; }
Record::Record(string &data) {
    vector<string> tokens;
    string token;
    istringstream token_stream(data);

    while (getline(token_stream, token, ',')) {
        tokens.push_back(token);
    }

    this -> id = stoi(tokens.at(0));
    this -> name = tokens.at(1);
    this -> ownership = tokens.at(2);
    this -> phone_number = tokens.at(3);
    this -> address = tokens.at(4);
    this -> city = tokens.at(5);
    this -> county = tokens.at(6);
    this -> zip = stoi(tokens.at(7));
    this -> coordinates = tokens.at(8);
    this -> latitude = stod(tokens.at(8).substr(0, 7));
    //this -> longitude = tokens.at(8).substr(9, tokens.at(8).length());
    this -> state = tokens.at(9);
    this -> image = tokens.at(10);
    this -> reg_hours = tokens.at(11);
    this -> sat_hours = tokens.at(12);
    this -> sun_hours = tokens.at(13);
    this -> is_open_24_hours = (tokens.at(14) == "Yes") ? 1 : 0;
    this -> has_clover_brew = (tokens.at(15) == "Yes") ? 1 : 0;
    this -> has_warmed_food = (tokens.at(16) == "Yes") ? 1 : 0;
    this -> has_free_wifi = (tokens.at(17) == "Yes") ? 1 : 0;
    this -> has_verismo_system = (tokens.at(18) == "Yes") ? 1 : 0;
    this -> allows_mobile_pay = (tokens.at(19) == "Yes") ? 1 : 0;
    this -> has_rewards = (tokens.at(20) == "Yes") ? 1 : 0;
    this -> has_la_boulange = (tokens.at(21) == "Yes") ? 1 : 0;
    this -> has_sodas = (tokens.at(22) == "Yes") ? 1 : 0;
    this -> has_drive_thru = (tokens.at(23) == "Yes") ? 1 : 0;
    this -> link = tokens.at(24);
}